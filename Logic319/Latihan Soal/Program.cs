﻿
//lingkaran();
//Persegi();
//modulus();
//gradeNilai();
//ganjilGenap();
//batangRokok();
//persegiSwitch();
lingkaranSwitch();


    static void lingkaran()
    {
    Console.WriteLine("Menghitung Keliling Lingkaran");
    Console.WriteLine();
    Console.Write("Masukkan Jari2 Lingkaran : ");
    int r = int.Parse(Console.ReadLine());
    double kel = 2 * 3.14 * r;
    double luas = 3.14 * r * r;
    kel = Convert.ToInt32(kel);
    luas = Convert.ToInt16(luas);
    Console.WriteLine(kel);
    Console.WriteLine(luas);
}

static void Persegi()
{
    Console.WriteLine("Mencari Keliling dan Luas Persegi");
    Console.Write("Masukkan Sisi Persegi : ");
    int s = int.Parse(Console.ReadLine());
    int Luas = s * s;
    int Kel = 4 * s;

    Console.WriteLine($"Luas Persegi = {Luas}");
    Console.WriteLine($"Keliling Persegi = {Kel} ");
}

static void modulus()
{
    Console.WriteLine("Mencari Modulus 0");
    Console.WriteLine();
    Console.Write("Masukkan Angka Pertama ");
    int a = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Angka Kedua ");
    int b = int.Parse(Console.ReadLine());
    int c = a % b;

    if ( a % b == 0 )
    {
        Console.WriteLine($"{a} % {b} adalah 0");
    }
    else { Console.WriteLine($"{a} % {b} bukan 0, melainkan {c}"); }
}

static void gradeNilai()
{
    Console.WriteLine("Menentukan grade nilai");
    Console.WriteLine();
    Console.Write("Masukkan Nilai disini : ");
    int nilai = int.Parse(Console.ReadLine());

    if (nilai >= 101)
    {
        Console.WriteLine("Grade = N/A ");
    }
    else if (nilai >= 80)
    {
        Console.WriteLine("Grade = A ");
    }
    else if (nilai >= 60)
    {
        Console.WriteLine("Grade = B ");
    }
    else if (nilai > 0 )
    {
        Console.WriteLine("Grade = C ");
    }
    else
    {
        Console.WriteLine("Grade = N/A");
    }
}

static void ganjilGenap()
{
    Console.WriteLine("Menentukan Ganjil atau Genap");
    Console.WriteLine();
    Console.Write("Masukkan angka disini : ");
    int angka = int.Parse(Console.ReadLine());

    if (angka % 2 == 0)
    {
        Console.WriteLine($"{angka} merupakan bilangan genap");
    }
    else {
        Console.WriteLine($"{angka} merupakan bilangan ganjil");
    }
}


static void batangRokok()
{
    Console.WriteLine("Menentukan Batang Rokoknya si pengemis");
    Console.WriteLine();
    Console.Write("Masukkan angka puntung : ");
    int puntung = int.Parse(Console.ReadLine());

    int batang = puntung / 8;
    int mod = puntung % 8; 
    int total = batang;
    int uang = total * 500;
    Console.WriteLine($"Total yang dimiliki pemulung adalah {total}, dengan sisa {mod} puntung");
    Console.WriteLine($"Total yang didapatkan pemulung Rp.{uang}");
}

static void persegiSwitch()
{
    Console.WriteLine("Mencari Keliling dan Luas Persegi");
    Console.Write("Pilih yang Anda cari (Luas / Keliling):  ");
    string pilihan = Console.ReadLine().ToLower();
    switch (pilihan)
    {
        case "luas":
            Console.WriteLine("anda mencari luas persegi");
            break;
        case "keliling":
            Console.WriteLine("anda mencari keliling persegi");
            break;
        default:
            Console.WriteLine("Cuman Luas dan Keliling :)");
            break;
    }

    Console.Write("Masukkan Sisi Persegi : ");
    int s = int.Parse(Console.ReadLine());


    if (pilihan == "luas")
    {
        int Luas = s * s;
        Console.WriteLine($"Luas Persegi = {Luas}");
    }
    else if (pilihan == "keliling") 
    {
        int Kel = 4 * s;
        Console.WriteLine($"Keliling Persegi = {Kel} ");
    }
    else
    {
        Console.WriteLine("N/A");
    }
}


static void lingkaranSwitch()
{
    Console.WriteLine("Menghitung Keliling Lingkaran");
    Console.WriteLine();

    Console.Write("Pilih yang Anda cari (Luas / Keliling):  ");
    string pilihan = Console.ReadLine().ToLower();
    switch (pilihan)
    {
        case "luas":
            Console.WriteLine("anda mencari luas Lingkaran");
            break;
        case "keliling":
            Console.WriteLine("anda mencari keliling Lingkaran");
            break;
        default:
            Console.WriteLine("Cuman Luas dan Keliling :)");
            break;
    }

    Console.Write("Masukkan Jari2 Lingkaran : ");
    int r = int.Parse(Console.ReadLine());
    double kel = 2 * 3.14 * r;
    double luas = 3.14 * r * r;
    if (pilihan == "luas")
    {

        luas = Convert.ToInt16(luas);
        Console.WriteLine($"Luas Lingkaran = {luas}");
    }
    else if (pilihan == "keliling")
    {

        kel = Convert.ToInt32(kel);
        Console.WriteLine($"Keliling Lingkaran = {kel} ");
    }
    else
    {
        Console.WriteLine("N/A");
    }
}

