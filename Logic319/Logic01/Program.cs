﻿
//Konversi();
//operatoAritmatika();
//operatoModulus();
//operatorPerbandingan();
using System.Reflection.Metadata.Ecma335;

operatorLogic();
Console.ReadKey();

static void Konversi()
{
    Console.WriteLine("==KONVERSI==");
    int myInt = 10;
    double myDouble = 5.25;
    bool myBool = true;

    string strMyInt = Convert.ToString(myInt);
    string strMyInt2 = myInt.ToString();

   
    Console.WriteLine(strMyInt);
    Console.WriteLine(strMyInt2);

    Console.WriteLine("============");

    Console.WriteLine(Convert.ToDouble(myInt));
    Console.WriteLine(Convert.ToInt32(myDouble));
    Console.WriteLine(Convert.ToString(myBool));

    Console.WriteLine(5 - -50);
}

static void operatoAritmatika()
{
    int mangga, apel, hasil = 0;

    Console.WriteLine("--OperatorAritmatika--");
    Console.Write("Masukkan Mangga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukkan Apel : ");
    apel=int.Parse(Console.ReadLine());

    hasil = mangga + apel;

    Console.WriteLine($"Hasil Manga + Apel = {hasil}");

}

static void operatoModulus()
{
    int jeruk, orang, hasil = 0;
    Console.WriteLine("Modulus");
    Console.WriteLine("Berapa Jumlah Mangga? ");
    jeruk = int.Parse(Console.ReadLine());
    Console.WriteLine("Berapa Jumlah Orang? ");
    orang=int.Parse(Console.ReadLine());    

    hasil= jeruk % orang;

    Console.WriteLine($"Jumlah Sisa Jeruk adalah {hasil}");
}

static void operatorPenugasan()
{
    int mangga = 10, apel = 8;

    mangga = 15;
    Console.WriteLine("==Operator Penugasan==");
    Console.WriteLine($"Mangga : {mangga}");

    apel = +6;
    Console.WriteLine($"Apel : {apel}");

}

static void operatorPerbandingan()
{ 
    int mangga = 0, apel = 0;
    Console.WriteLine("Perbandingan");
    Console.Write("Masukkan managga : ");
    mangga = int.Parse(Console.ReadLine());
    Console.WriteLine("Masukkan apel : ");
    apel = int.Parse(Console.ReadLine());

    Console.WriteLine("Hasil Perbandingan : ");
    Console.WriteLine($"MAngga > Apel : {mangga > apel}");
    Console.WriteLine($"MAngga >= Apel : {mangga >= apel}");
    Console.WriteLine($"MAngga < Apel : {mangga < apel}");
    Console.WriteLine($"MAngga <= Apel : {mangga <= apel}");
    Console.WriteLine($"MAngga == Apel : {mangga == apel}");
    Console.WriteLine($"MAngga != Apel : {mangga != apel}");

}

static void operatorLogic()
{
    Console.WriteLine("--Operator Logic--");
    Console.WriteLine("Enter Your Age : ");
    int age = int.Parse(Console.ReadLine());
    Console.Write("Password : ");
    string password = Console.ReadLine(); ;

    bool isAdult = age >= 18;
    bool isPasswordValid = password == "Admin";

    if (isAdult && isPasswordValid)
    {
        Console.WriteLine("Welcome To The Club");
    }
    else
    {
        Console.WriteLine("Not Welcome");
    }

}

static int hasil(int mangga, int apel)
{
    int hasil = 0;
    hasil = mangga + apel;
    return hasil;
}
static void methodReturnType()
{
    Console.WriteLine("--MEthode Return Type--");
    Console.WriteLine("Masukan Mangga : ");
    int mangga = int.Parse(Console.ReadLine()) ; ;
    Console.Write("Masukan Apel : ");
    int apel = int.Parse(Console.ReadLine()); ;

    int jumlah = hasil(mangga, apel);
    Console.WriteLine($"mangga dan apel = {0}",jumlah);

}