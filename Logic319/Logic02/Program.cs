﻿//ifStatement();
//ifelseStatment();
//ifelifStatment();
//nestedIf();
//ternaty();
Switch();

Console.ReadKey();

static void ifStatement()
{
    Console.WriteLine("==IF STATEMENT==");
    Console.Write  ("Masukan niali X : ");
    int x = int.Parse(Console.ReadLine());
    Console.WriteLine("Masukan Nilai Y : ");
    int y = int.Parse(Console.ReadLine());

    if (x <= 10)
    {
        Console.WriteLine("X is lesser than 10");    
    }
    if (y <= 5 )
    {
        Console.WriteLine("y is lesser than 5");
    }
}

static void ifelseStatment()
{
    Console.WriteLine("==IFELSE STATEMENT==");
    Console.Write("Masukan Nilai X: ");
    int x = int.Parse (Console.ReadLine());
    Console.WriteLine("Masukan Nilai Y : ");
    int y = int.Parse (Console.ReadLine()); 

    if (x <= y)
    {
        Console.WriteLine(" X Lebih kecil sama dengan Y");
    }
    else {
        Console.WriteLine("X Lebih Besar dari Y");
    }
}

static void ifelifStatment()
{
    Console.WriteLine("==IFELIF STATEMENT==");
    Console.WriteLine("Masukan Nilai X : ");
    int x = int.Parse(Console.ReadLine());

    if (x == 10)
    {
        Console.WriteLine("x sama dengan 10");
    }
    else if (x < 10)
    {
        Console.WriteLine("X lebih kecil dari 10");
    }
    else { 
        Console.WriteLine("X lebih besar dari 10");
    }
}

static void nestedIf() 
{ 
    Console.WriteLine("==Nested If Statement==");
    Console.WriteLine("Masukan Nilai X : ");
    int x = int.Parse(Console.ReadLine());

    if (x % 2 == 0)
    {

        if (x < 10)
        {
            Console.WriteLine("X adalah bilangan genap dan lebih kecil dari 10");
        }
        else {
            Console.WriteLine("X adalah bilangan genap dan lebih besar dari 10");
        }
    }
    else if (x % 2 != 0)
    {
        if (x < 10)
        {
            Console.WriteLine("X adalah bilangan ganjil dan lebih kecil dari 10");
        }
        else
        {
            Console.WriteLine("X adalah bilangan ganjil dan lebih besar dari 10");
        }
    }
    else 
    { 
            Console.WriteLine("N/A");
    }
}

static void ternaty ()
{
    Console.WriteLine("Ternary");
    Console.WriteLine("Masukan Inputan X : ");
    int x = int.Parse(Console.ReadLine());
    Console.WriteLine("Masukan Inputan Y : ");
    int y = int.Parse(Console.ReadLine());
    string z;


    if (y > x)
    { 
        Console.WriteLine(" Y lebih besar dari X");
    }
    else {
        Console.WriteLine("Y lebih besar dari X");    
    }

    z = (y > x) ? "y lebih besar dari pada x" : "x lenbih besar dari pada y";
    Console.WriteLine(z);

   
}

static void Switch()

{

    Console.Write("Pilih buah kesukaan Anda (Apel/Jeruk/Pisang):  ");
    string pilihan = Console.ReadLine().ToLower();

    switch(pilihan)
    {
        case "apel":
            Console.WriteLine("anda memilih buah apel");
            break;
        case "jeruk":
            Console.WriteLine("anda memilih buah jeruk");
            break;
        case "pisang":
            Console.WriteLine("anda memilih buah pisang");
            break;
        default: Console.WriteLine("anda milih buah apa sih?");
            break;
    }
    if (pilihan == "pisang")
    {
        Console.WriteLine("yeee monyet emang");
    }
    else {
        Console.WriteLine("Bagus biar sehat makan buah");
    }
}
   


    